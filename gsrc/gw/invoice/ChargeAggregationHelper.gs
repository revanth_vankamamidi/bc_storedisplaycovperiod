package gw.invoice

uses com.guidewire.bc.domain.invoice.impl.InvoiceLineItemImpl
/**
 * Created with IntelliJ IDEA.
 * User: Revanth.Vankamamidi
 * Date: 23/5/16
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */
class ChargeAggregationHelper {
  static function getAggregatedInvoiceItems(invoice: AccountInvoice, aggregation: AggregationType ): InvoiceLineItem[] {

    if(aggregation != AggregationType.TC_CHARGEDATES)
      return invoice.getAggregatedInvoiceItems(aggregation)

    var invoiceLineItemArray = invoice.getAggregatedInvoiceItems(AggregationType.TC_CHARGES)
    var finalInvoiceLineItemArray = new InvoiceLineItemImpl[invoiceLineItemArray.length];

    // search in the second array if there is a row with the combination of
    // policynumber-product-chargeeffectivedate and chargeexpiry date, if not present, add the row, else add up the values and save back into the second array

    var tempCounter = 0


    for(var tempSrcInvoiceItem in invoiceLineItemArray) {

      var itemTakenCare = false;
      for(var tempDestInvoiceItem in finalInvoiceLineItemArray) {
        if(tempSrcInvoiceItem.PolicyPeriod.DisplayName == tempDestInvoiceItem.PolicyPeriod.DisplayName
            and tempSrcInvoiceItem.PolicyPeriod.Policy.LOBCode == tempDestInvoiceItem.PolicyPeriod.Policy.LOBCode
            and tempSrcInvoiceItem.Charge.EffectiveDate == tempDestInvoiceItem.Charge.EffectiveDate
            and tempSrcInvoiceItem.Charge.ExpirationDate == tempDestInvoiceItem.Charge.ExpirationDate ) {

          tempDestInvoiceItem.Amount = tempDestInvoiceItem.Amount  + tempSrcInvoiceItem.Amount
          itemTakenCare=true
        }
      }

      if(!itemTakenCare) {
        finalInvoiceLineItemArray[tempCounter]  =   (InvoiceLineItemImpl) tempSrcInvoiceItem
        tempCounter++
      }
    }

    var realFinalInvoiceLineItemArray = new InvoiceLineItemImpl[tempCounter]
    for(var tempSrcInvoiceItem in finalInvoiceLineItemArray index newCounter) {
      realFinalInvoiceLineItemArray[newCounter] = tempSrcInvoiceItem
    }

    return realFinalInvoiceLineItemArray
  }

}